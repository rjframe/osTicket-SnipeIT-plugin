<?php

return array(
    'id' =>             'snipeit',
    'version' =>        '0.1',
    'name' =>           'Snipe-IT Asset Management Integrator',
    'author' =>         'Ryan Frame',
    'description' =>    'Enables linking assets to tickets',
    'url' =>            'TODO',
    'plugin' =>         'snipeit.php:SnipeITPlugin'
);

?>
