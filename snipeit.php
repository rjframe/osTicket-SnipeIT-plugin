<?php
    /* - this goes on the ticket. Wherever that will be...
    array(
        'label' => 'Asset Tag',
        'hint' => 'Link this ticket to the specified asset',
        'configuration' => array('size' => 60, 'length' => 255),
        'required' => true
    )
     */

require_once(INCLUDE_DIR.'class.plugin.php');
require_once(INCLUDE_DIR.'class.signal.php');
require_once(INCLUDE_DIR.'class.ticket.php');

class SnipeITPluginConfig extends PluginConfig {
    function getOptions() {
        return array(
            // TODO: Snipe-IT API data.
            'snipeit-host' => new TextboxField(
                array(
                    'label' => 'Snipe-IT URL',
                    'hint' => 'Include the http[s]:// portion.',
                    'configuration' => array(
                        'size' => 60, 'length' => 100
                    ),
                    'required' => true
                )
            ),
            'snipeit-token' => new TextboxField(
                array(
                    'label' => 'Snipe-IT API token',
                    'hint' => 'Generate a key via Snipe-IT\'s "Manage API Keys" menu.',
                    'configuration' => array(
                        'size' => 60, 'length' => 1071
                    ),
                    'required' => true
                )
            )
        );
    }

    function pre_save($config, &$errors) {
        // Verify we can connect to Snipe-IT.
        $url = $config['snipeit-host'].'/api/v1/users/1';

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $httpRequest,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer '.$config['snipeit-token']
            ),
            CURLOPT_USERAGENT => 'snipeit-osticket-plugin/0.1 (snipeit@ryanjframe.com)'
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($response, true);

        if ($json["id"] == "1") {
            return true;
        } else {
            $errors['err'] = 'Validation failed; please check your configuration and API key.';
            return false;
        }
    }
}

class SnipeITPlugin extends Plugin {
    var $config_class = 'SnipeITPluginConfig';

    var $api = '';

    function bootstrap() {
        $config = $this->getConfig();
        $this->api = $this->config['snipeit-host'].'/api/v1/';

        $this->doFirstRunSetup();

        //Signal::connect('model.created', array($this, 'validateAssetTag'), 'Ticket');

        Signal::connect('ticket.create.before', array($this, 'validateAssetTag'));
    }

    // I don't see an installation hook in Plugin... TODO: double-check and PR.
    function dofirstRunSetup() {
        $snipe_col_exists =
                db_result(db_query("SHOW COLUMNS FROM ".TICKET_TABLE." LIKE 'snipe_asset'"));

        if (! $snipe_col_exists) {
            // NOTE: I believe this is a MySQL-only query.
            db_query('ALTER TABLE '.TICKET_TABLE.' ADD snipe_asset VARCHAR(30) AFTER flags');
        }
    }

    public function get($uri) {
        global $msg;
        $msg = ($this->api).$uri;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => ($this->api).$uri,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $httpRequest,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'Authorization: Bearer '.$object->config['snipeit-token']
            ),
            CURLOPT_USERAGENT => 'snipeit-osticket-plugin/0.1 (snipeit@ryanjframe.com)'
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        //return json_decode($response, true);
        return $response;
    }

    public function validateAssetTag($data) {
        // TODO: Figure this out.
        //print($data);
        //print($data['snipe_asset']);
        //$data['snipe-asset'] = 'Works!';

        // TODO: Do I just return false? Generate an error?
        // TODO: $response = $object->send('/hardware/bytag/'.$assetTag, 'GET');
        // return $response['status'] != 'error';
    }
}

?>
